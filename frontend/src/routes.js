import {
    DASHBOARD_ROUTE,
    LOGIN_ROUTE, MAIN
} from "./utils/const";
import Auth from "./page/Auth";
import Main from "./page/Main";

export const authRoutes = [
    {
        path: DASHBOARD_ROUTE,
        Component: Main
    }
]

export const publicRoutes = [
    {
        path: LOGIN_ROUTE,
        Component: Auth
    },
    {
        path: MAIN,
        Component: Main
    }
]