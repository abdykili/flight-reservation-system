/* eslint-disable */
import React, {useContext, useEffect, useState} from 'react';

const Dashboard = () => {

    const [fromCountry,setFromCountry] = useState('')
    const [toCountry,setToCountry] = useState('')
    const [date,setDate] = useState('')
    const [foundFlights,setFoundFlights] = useState([])


    const searchFlights = async () => {
        try {
            let response
            let string = searchInput.split(" ");
            let name = string[0];
            let surname = string[1];
            response = await filterFlights(fromCountry, toCountry, date)
            setFoundFlights(response)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <div>
            <header>
                <div className="wrapper">
                    <h1>Flight reservation system</h1>
                </div>
            </header>
            <div className="row">
                <div className="wrapper">
                    <div className="filter">
                        <div className="filter-item">
                            <input type="text" placeholder="From: City"/>
                        </div>
                        <div className="filter-item">
                            <input type="text" placeholder="To: City"/>
                        </div>
                        <div className="filter-item">
                            <input type="text" placeholder="When: dd/mm/yy"/>
                        </div>
                        <div className="filter-item">
                            <button  type="button"
                                    className="btn btn-primary waves-effect waves-light">Find flights
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="wrapper">
                    <div className="flights">
                        <div className="row">
                            { foundFlights.length === 0 ?
                                "Empty flights"
                                :
                                foundFlights.map(contact =>
                                    <div className="flight-item col-md-12">
                                        flight
                                    </div>
                                )
                            }

                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
};

export default Dashboard;