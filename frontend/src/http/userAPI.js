import {$authHost, $host} from "./index";
import jwtDecode from 'jwt-decode';

export const login = async (username, password) =>{
    const {data} = await $host.post('/api/v1/user/login/', {username, password})
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}

export const auth = async () => {
    const {data} = await $authHost.get('/api/v1/user/refresh/')
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}