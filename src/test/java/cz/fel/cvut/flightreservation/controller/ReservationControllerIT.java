package cz.fel.cvut.flightreservation.controller;

import cz.fel.cvut.flightreservation.domain.ReservationState;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ReservationRequestDto;
import cz.fel.cvut.flightreservation.model.response.ReservationResponseDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Objects;

/**
 * Integration tests for reservation controller
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void createReservationByUnregistered(){
        // arrange
        final ReservationRequestDto reservationRequestDto = new ReservationRequestDto()
                .setFlightId(1L)
                .setPassengers(Arrays.asList(new PassengerRequestDto().setSeatId(1L)))
                .setFlightClass("Business");

        // act
        final ResponseEntity<ReservationResponseDto> response = testRestTemplate.postForEntity("/api/v1/reservations/unregistered", reservationRequestDto, ReservationResponseDto.class);
        final HttpStatus code = response.getStatusCode();

        // assert
        assertThat(code).isEqualTo(HttpStatus.CREATED);
    }
}
