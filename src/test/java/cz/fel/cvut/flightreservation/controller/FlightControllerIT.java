package cz.fel.cvut.flightreservation.controller;

import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ReservationRequestDto;
import cz.fel.cvut.flightreservation.model.response.ReservationResponseDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

/**
 * Integration tests for flight controller
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FlightControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void createFlightIntegrationTest(){
        // arrange
        final FlightRequestDto flightRequestDto = new FlightRequestDto()
                .setAirportArrivalId(1L)
                .setAirportDepartureId(2L)
                .setAircraftId(1L);

        // act
        final ResponseEntity<Void> response = testRestTemplate.postForEntity("/api/v1/flights/", flightRequestDto, null);
        final HttpStatus code = response.getStatusCode();

        // assert
        assertThat(code).isEqualTo(HttpStatus.CREATED);
    }
}