package cz.fel.cvut.flightreservation.controller;

import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration tests for flight controller
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AircraftControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void createAircraftIntegrationTest(){
        // arrange
        final AircraftRequestDto aircraftRequestDto = new AircraftRequestDto()
                .setAircraftModel("T222")
                .setSeatsCount(24);

        // act
        final ResponseEntity<Void> response = testRestTemplate.postForEntity("/api/v1/aircrafts/", aircraftRequestDto, null);
        final HttpStatus code = response.getStatusCode();

        // assert
        assertThat(code).isEqualTo(HttpStatus.CREATED);
    }
}