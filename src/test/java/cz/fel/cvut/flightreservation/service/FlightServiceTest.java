package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.domain.AircraftEntity;
import cz.fel.cvut.flightreservation.domain.AirportEntity;
import cz.fel.cvut.flightreservation.domain.FlightEntity;
import cz.fel.cvut.flightreservation.mapper.FlightMapper;
import cz.fel.cvut.flightreservation.mapper.FlightRepository;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.repository.AircraftRepository;
import cz.fel.cvut.flightreservation.repository.AirportRepository;
import cz.fel.cvut.flightreservation.repository.SeatRepository;
import cz.fel.cvut.flightreservation.service.impl.FlightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private AirportRepository airportRepository;

    @Mock
    private AircraftRepository aircraftRepository;

    @Mock
    private SeatRepository seatRepository;

    @Mock
    private FlightMapper flightMapper;

    @InjectMocks
    private FlightServiceImpl flightService;

    @Test
    void createFlight(){
        // arrange
        final FlightRequestDto flightRequestDto = new FlightRequestDto()
                .setAircraftId(1L)
                .setFlightCode("T232")
                .setAirportArrivalId(1L)
                .setAirportDepartureId(2L);
        final AircraftEntity aircraft = new AircraftEntity()
                .setSeatsCount(24);
        final AirportEntity airportArrival = (AirportEntity) new AirportEntity()
                .setId(1L);
        final AirportEntity airportDeparture = (AirportEntity) new AirportEntity()
                .setId(2L);
        final FlightEntity flight = new FlightEntity()
                .setFlightCode("T232");
        when(airportRepository.findById(1L)).thenReturn(java.util.Optional.of(airportArrival));
        when(aircraftRepository.findById(1L)).thenReturn(java.util.Optional.of(aircraft));
        when(airportRepository.findById(2L)).thenReturn(java.util.Optional.of(airportDeparture));
        when(flightMapper.toEntity(flightRequestDto)).thenReturn(flight);
        when(flightRepository.save(flight)).thenReturn(flight);
        // act
        flightService.addFlight(flightRequestDto);
        // assert
        verify(aircraftRepository).findById(1L);
        verify(airportRepository).findById(1L);
        verify(airportRepository).findById(2L);
        verify(flightMapper).toEntity(eq(flightRequestDto));
        verify(flightRepository).save(eq(flight));
    }

}
