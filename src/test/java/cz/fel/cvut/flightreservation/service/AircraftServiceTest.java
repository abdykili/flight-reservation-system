package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.domain.AircraftEntity;
import cz.fel.cvut.flightreservation.mapper.AircraftMapper;
import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.repository.AircraftRepository;
import cz.fel.cvut.flightreservation.service.impl.AircraftServiceImpl;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AircraftServiceTest implements WithAssertions {

    @Mock
    private AircraftMapper aircraftMapper;

    @Mock
    private AircraftRepository aircraftRepository;

    @InjectMocks
    private AircraftServiceImpl aircraftService;

    @Test
    void createAircraft(){
        // arrange
        final AircraftRequestDto aircraftRequestDto = new AircraftRequestDto()
                .setAircraftModel("T252")
                .setSeatsCount(24);
        final AircraftEntity aircraft = new AircraftEntity();
        when(aircraftMapper.toEntity(aircraftRequestDto)).thenReturn(aircraft);
        // act
        aircraftService.createAircraft(aircraftRequestDto);
        // assert
        verify(aircraftMapper).toEntity(aircraftRequestDto);
        verify(aircraftRepository).save(aircraft);
    }
}
