package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.domain.*;
import cz.fel.cvut.flightreservation.mapper.FlightRepository;
import cz.fel.cvut.flightreservation.mapper.PassengerMapper;
import cz.fel.cvut.flightreservation.mapper.ReservationMapper;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ReservationRequestDto;
import cz.fel.cvut.flightreservation.model.response.SeatResponseDto;
import cz.fel.cvut.flightreservation.repository.PassengerRepository;
import cz.fel.cvut.flightreservation.repository.ReservationRepository;
import cz.fel.cvut.flightreservation.repository.SeatRepository;
import cz.fel.cvut.flightreservation.service.impl.ReservationServiceImpl;
import cz.fel.cvut.flightreservation.service.impl.SeatServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

    @Mock
    private ReservationMapper reservationMapper;

    @Mock
    private SeatServiceImpl seatService;

    @Mock
    private SeatRepository seatRepository;

    @Mock
    private PassengerMapper passengerMapper;

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private PassengerRepository passengerRepository;

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private ReservationServiceImpl reservationService;

    @Test
    void createReservationByRegisteredCustomer(){
    }

    @Test
    void createReservationByRegisteredCustomer_ThrowsEntityNotFound(){

    }

    @Test
    void createReservationByUnregisteredCustomer(){
        // arrange
        final SeatResponseDto seatResponseDto = new SeatResponseDto()
                .setSeatName("1L")
                .setSeatState(String.valueOf(SeatState.BORROWED));

        final SeatEntity seatEntity = (SeatEntity) new SeatEntity().setId(1L);
        final ReservationRequestDto reservationRequestDto1 = new ReservationRequestDto()
                .setPassengers(Arrays.asList(new PassengerRequestDto()
                        .setName("Passenger1")
                        .setSeatId(1L)
                ))
                .setFlightClass("Business")
                .setFlightId(1L);
        final ReservationEntity reservationEntity = new ReservationEntity();
        final FlightEntity flightEntity = (FlightEntity) new FlightEntity()
                .setId(1L);
        final PassengerEntity passengerEntity = (PassengerEntity) new PassengerEntity()
                .setName("Passenger1")
                .setId(1L);

        when(reservationMapper.toEntity(reservationRequestDto1)).thenReturn(reservationEntity);
        when(flightRepository.findById(1L)).thenReturn(java.util.Optional.of(flightEntity));
        when(reservationRepository.save(reservationEntity)).thenReturn(reservationEntity);
        when(passengerRepository.save(passengerEntity)).thenReturn(passengerEntity);
        when(passengerMapper.toEntity(any(PassengerRequestDto.class))).thenReturn(passengerEntity);
        when(seatRepository.findById(anyLong())).thenReturn(java.util.Optional.of(seatEntity));
        when(seatService.reserveFlightSeat(anyLong(), anyLong(), anyLong())).thenReturn(seatResponseDto);

        // act
        reservationService.createReservationByUnregisteredCustomer(reservationRequestDto1);

        // access
        verify(reservationMapper).toEntity(eq(reservationRequestDto1));
        verify(flightRepository).findById(eq(1L));
        verify(reservationRepository).save(eq(reservationEntity));
        verify(seatRepository).findById(anyLong());
        verify(seatService).reserveFlightSeat(anyLong(), anyLong(), anyLong());
    }
}