package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.model.request.AdminRequestDto;
import cz.fel.cvut.flightreservation.model.request.CustomerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ManagerRequestDto;
import cz.fel.cvut.flightreservation.model.response.AdminResponseDto;
import cz.fel.cvut.flightreservation.model.response.CustomerResponseDto;
import cz.fel.cvut.flightreservation.model.response.ManagerResponseDto;

public interface UserService {
    AdminResponseDto saveAdmin(AdminRequestDto adminRequestDto);

    ManagerResponseDto saveManager(ManagerRequestDto user);

    CustomerResponseDto saveCustomer(CustomerRequestDto customerRequestDto);

    void testData();
}
