package cz.fel.cvut.flightreservation.service.impl;

import cz.fel.cvut.flightreservation.domain.FlightEntity;
import cz.fel.cvut.flightreservation.domain.PassengerEntity;
import cz.fel.cvut.flightreservation.domain.SeatEntity;
import cz.fel.cvut.flightreservation.domain.SeatState;
import cz.fel.cvut.flightreservation.mapper.FlightRepository;
import cz.fel.cvut.flightreservation.mapper.SeatMapper;
import cz.fel.cvut.flightreservation.model.response.SeatResponseDto;
import cz.fel.cvut.flightreservation.repository.PassengerRepository;
import cz.fel.cvut.flightreservation.repository.SeatRepository;
import cz.fel.cvut.flightreservation.service.SeatService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SeatServiceImpl implements SeatService {

    private final SeatMapper seatMapper;
    private final SeatRepository seatRepository;
    private final FlightRepository flightRepository;
    private final PassengerRepository passengerRepository;

    @Override
    @Transactional
    public SeatResponseDto reserveFlightSeat(Long seatId, Long passengerId, Long flightId) {
        log.info("Reserving flight code #{} seat | Passenger id {} | Flight id {}", flightId, passengerId, seatId);

        final SeatEntity seat = seatRepository.findById(seatId)
                .orElseThrow(() -> new EntityNotFoundException("Seat doesnt exist"));
        final FlightEntity flight = flightRepository.findById(flightId)
                .orElseThrow(() -> new EntityNotFoundException("Flight with such id doesnt exist"));
        if(seat.getFlight().getId() != flight.getId()) throw new RuntimeException("Seat and flight ids are not the same!");
        final PassengerEntity passengerEntity = passengerRepository.findById(passengerId)
                .orElseThrow(() -> new EntityNotFoundException("Passenger doesnt exist"));
        seat.setSeatState(SeatState.BORROWED);
        seat.setPassengerEntity(passengerEntity);
        passengerEntity.setSeatEntity(seat);

        final SeatEntity save = seatRepository.save(seat);
        final SeatResponseDto seatResponseDto = seatMapper.toResponse(save);
        log.info("Seat state successfully changed to BORROWED | Seat response - {}", seatResponseDto);
        return seatResponseDto;
    }
}
