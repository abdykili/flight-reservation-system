package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.model.response.SeatResponseDto;

public interface SeatService {

    SeatResponseDto reserveFlightSeat(Long seatId, Long passengerId, Long flightId);

}
