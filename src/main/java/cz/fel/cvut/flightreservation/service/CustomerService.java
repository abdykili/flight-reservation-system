package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.model.request.CustomerRequestDto;
import cz.fel.cvut.flightreservation.model.response.CustomerResponseDto;

import java.util.List;

/**
 * Registered customer crud service
 */
public interface CustomerService {

    /**
     * Creates new customer
     * @param customerRequestDto
     */
    void createCustomer(CustomerRequestDto customerRequestDto);

    void deleteCustomer(Long id);

    CustomerResponseDto updateCustomer(Long customerId, CustomerRequestDto customerRequestDto);

    List<CustomerResponseDto> findAll();

    CustomerResponseDto findById(Long id);
}
