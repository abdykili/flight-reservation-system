package cz.fel.cvut.flightreservation.service.impl;

import cz.fel.cvut.flightreservation.criteriaAPI.FlightDAO;
import cz.fel.cvut.flightreservation.criteriaAPI.SearchCriteria;
import cz.fel.cvut.flightreservation.domain.*;
import cz.fel.cvut.flightreservation.mapper.FlightMapper;
import cz.fel.cvut.flightreservation.mapper.FlightRepository;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;
import cz.fel.cvut.flightreservation.repository.AircraftRepository;
import cz.fel.cvut.flightreservation.repository.AirportRepository;
import cz.fel.cvut.flightreservation.repository.SeatRepository;
import cz.fel.cvut.flightreservation.service.FlightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;
    private final AirportRepository airportRepository;
    private final AircraftRepository aircraftRepository;
    private final SeatRepository seatRepository;
    private final FlightMapper flightMapper;
    private final FlightDAO flightDAO;

    @Override
    @Transactional
    public void addFlight(FlightRequestDto flightRequestDto) {
        log.info("Adding new flight to system | Request dto {}", flightRequestDto);
        final AircraftEntity aircraft = aircraftRepository.findById(flightRequestDto.getAircraftId())
                .orElseThrow(() -> new EntityNotFoundException("Aircraft doesnt exist"));
        final AirportEntity departureAirport = airportRepository.findById(flightRequestDto.getAirportDepartureId())
                .orElseThrow(() -> new EntityNotFoundException("Departure airport doesnt exist"));
        final AirportEntity arrivalAirport = airportRepository.findById(flightRequestDto.getAirportArrivalId())
                .orElseThrow(() -> new EntityNotFoundException("Arrival airport doesnt exist"));
        if(arrivalAirport.getId() == departureAirport.getId())
            throw new RuntimeException("Arrival and departure airports are the same");

        final FlightEntity flightEntity = flightMapper.toEntity(flightRequestDto)
                .setAircraft(aircraft)
                .setAirportArrival(arrivalAirport)
                .setAirportDeparture(departureAirport)
                .setFromCountry(departureAirport.getCountry())
                .setToCountry(arrivalAirport.getCountry());
        final FlightEntity save = flightRepository.save(flightEntity);
        save.setSeats(generateSeats(aircraft.getSeatsCount(), save));
        log.info("Flight code {} is successfully created!", flightEntity.getFlightCode());
    }

    @Override
    public void updateFlight(Long flightId, FlightRequestDto flightRequestDto) {

    }

    @Override
    public void deleteFlight(Long flightId) {

    }

    @Override
    public FlightResponseDto findFlight(Long flightId) {
        return null;
    }

    private List<SeatEntity> generateSeats(int seatsCount, FlightEntity flight){
        List<SeatEntity> seatEntities = new ArrayList<SeatEntity>();
        log.info("Generating seats for new aircraft");

        for (int i = 1; i <= seatsCount; i++) {
            String seatName;
            if(i < seatsCount / 2){
                seatName = String.format("%dL", i);
            }else{
                seatName = String.format("%dR", i);
            }
            final SeatEntity seatEntity = new SeatEntity()
                    .setSeatState(SeatState.AVAILABLE)
                    .setSeatName(seatName)
                    .setFlight(flight);
            final SeatEntity save = seatRepository.save(seatEntity);
            seatEntities.add(save);
        }

        return seatEntities;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FlightResponseDto> findAll() {
        log.info("Returning all flights from database");
        final List<FlightEntity> all = flightRepository.findAll();
        return all.stream().map(flightMapper::toResponse).collect(Collectors.toList());
    }

    @Override
    public List<FlightResponseDto> filterFlights(List<SearchCriteria> params) {
        return flightDAO.searchFlights(params).stream()
                .map(flightMapper::toResponse)
                .collect(Collectors.toList());
    }
}
