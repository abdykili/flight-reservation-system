package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.model.request.ManagerRequestDto;
import cz.fel.cvut.flightreservation.model.request.NewPassengerRequestDto;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import cz.fel.cvut.flightreservation.model.response.ManagerResponseDto;
import cz.fel.cvut.flightreservation.model.response.PassengerResponseDto;

import java.util.List;

public interface PassengerService {

    void saveNewPassenger(NewPassengerRequestDto newPassengerRequestDto);

    void deletePassenger(Long id);

    void updatePassenger(Long passengerId, NewPassengerRequestDto newPassengerRequestDto);

    List<PassengerResponseDto> findAll();

    PassengerResponseDto findById(Long id);
}
