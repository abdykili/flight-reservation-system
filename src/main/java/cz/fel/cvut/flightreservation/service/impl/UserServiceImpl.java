package cz.fel.cvut.flightreservation.service.impl;

import cz.fel.cvut.flightreservation.domain.RoleEntity;
import cz.fel.cvut.flightreservation.domain.UserEntity;
import cz.fel.cvut.flightreservation.mapper.AdminMapper;
import cz.fel.cvut.flightreservation.mapper.CustomerMapper;
import cz.fel.cvut.flightreservation.mapper.ManagerMapper;
import cz.fel.cvut.flightreservation.model.request.AdminRequestDto;
import cz.fel.cvut.flightreservation.model.request.CustomerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ManagerRequestDto;
import cz.fel.cvut.flightreservation.model.response.AdminResponseDto;
import cz.fel.cvut.flightreservation.model.response.CustomerResponseDto;
import cz.fel.cvut.flightreservation.model.response.ManagerResponseDto;
import cz.fel.cvut.flightreservation.repository.*;
import cz.fel.cvut.flightreservation.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service(value = "userService")
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserDetailsService, UserService {

    private final UserRepository userRepository;
    private final ManagerMapper managerMapper;
    private final RoleRepository roleRepository;
    private final AdminMapper adminMapper;
    private final CustomerMapper customerMapper;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public AdminResponseDto saveAdmin(AdminRequestDto adminRequestDto) {
        log.info("Creating an admin | Admin request dto - {}", adminRequestDto);
        UserEntity user = adminMapper.toEntity(adminRequestDto);

        log.info("Creating an admin | Controlling if username is free");
        if (userRepository.existsByLogin(adminRequestDto.getLogin())) {
            throw new RuntimeException("Admin with such username exists");
        }
        user.setPassword(passwordEncoder.encode(adminRequestDto.getPassword()));

        RoleEntity role = roleRepository.findByName("ADMIN");
        log.info("Creating an new admin | Adding an admin role - {}", role);

        Set<RoleEntity> roleSet = new HashSet<>();
        roleSet.add(role);
        user.setRoles(roleSet);
        UserEntity save = userRepository.save(user);

        return adminMapper.toResponse(save);
    }

    @Override
    public ManagerResponseDto saveManager(ManagerRequestDto user) {
        return null;
    }

    @Override
    public CustomerResponseDto saveCustomer(CustomerRequestDto customerRequestDto) {
        return null;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username doesnt exist");
        }
        return new User(user.getLogin(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(UserEntity user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }

    @Transactional
    @Override
    public void testData() {
        // admin
        final AdminRequestDto adminRequestDto = new AdminRequestDto()
                .setLogin("admin")
                .setPassword("123");
        saveAdmin(adminRequestDto);
        // manager
        final ManagerRequestDto managerRequestDto1 = new ManagerRequestDto()
                .setLogin("manager")
                .setPassword("123")
                .setName("Tomas")
                .setSurname("Pavlivec");
        saveManager(managerRequestDto1);

        final CustomerRequestDto customerRequestDto = new CustomerRequestDto()
                .setLogin("customer")
                .setPassword("123")
                .setName("Mark")
                .setSurname("Pavlivec")
                .setBirthDate(LocalDate.of(2021, 10, 28))
                .setEmail("ilias.abdykarov@gmail.com")
                .setNationalId("2810011347")
                .setPass("AC3277891")
                .setPhone("32312312321");
        saveCustomer(customerRequestDto);
    }
}
