package cz.fel.cvut.flightreservation.service.impl;

import cz.fel.cvut.flightreservation.domain.*;
import cz.fel.cvut.flightreservation.mapper.FlightRepository;
import cz.fel.cvut.flightreservation.mapper.PassengerMapper;
import cz.fel.cvut.flightreservation.mapper.ReservationMapper;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ReservationRequestDto;
import cz.fel.cvut.flightreservation.model.response.ReservationResponseDto;
import cz.fel.cvut.flightreservation.model.response.SeatResponseDto;
import cz.fel.cvut.flightreservation.repository.PassengerRepository;
import cz.fel.cvut.flightreservation.repository.ReservationRepository;
import cz.fel.cvut.flightreservation.repository.SeatRepository;
import cz.fel.cvut.flightreservation.service.ReservationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    private final SeatServiceImpl seatService;
    private final SeatRepository seatRepository;
    private final ReservationMapper reservationMapper;
    private final PassengerMapper passengerMapper;
    private final ReservationRepository reservationRepository;
    private final PassengerRepository passengerRepository;
    private final FlightRepository flightRepository;

    @Override
    @Transactional
    public void createReservationByUnregisteredCustomer(ReservationRequestDto reservationRequestDto) {
        log.info("Creating reservation by unregistered customer");

        final ReservationEntity reservationEntity = reservationMapper.toEntity(reservationRequestDto)
                .setReservationState(ReservationState.NEW);
        final FlightEntity flightEntity = flightRepository.findById(reservationRequestDto.getFlightId())
                .orElseThrow(() -> new EntityNotFoundException("Flight doesnt exist"));
        reservationEntity.setFlight(flightEntity);
        reservationEntity.setCustomerUser(null);
        final ReservationEntity save = reservationRepository.save(reservationEntity);
        final List<PassengerRequestDto> passengers = reservationRequestDto.getPassengers();
        final Set<PassengerEntity> passengerEntities = passengers.stream()
                .map(passenger -> {
                    log.info("Creating new passenger");
                    final SeatEntity seatEntity = seatRepository.findById(passenger.getSeatId())
                            .orElseThrow(() -> new EntityNotFoundException("Seat doesnt exist"));
                    final PassengerEntity passengerEntity = passengerMapper.toEntity(passenger);
                    passengerEntity.setReservation(save);
                    final PassengerEntity savedPassenger = passengerRepository.save(passengerEntity);
                    final SeatResponseDto seatResponseDto = seatService.reserveFlightSeat(seatEntity.getId(),
                            savedPassenger.getId(), flightEntity.getId());
                    return savedPassenger;
                })
                .collect(Collectors.toSet());
        save.setPassengers(passengerEntities);

        log.info("Reservation successfully created");
    }

    @Override
    public void createReservationByRegisteredCustomer(Long customerId, ReservationRequestDto ReservationRequestDto) {

    }

    @Override
    public void deleteReservation(Long id) {

    }

    @Override
    public ReservationResponseDto updateReservation(Long ReservationId, ReservationRequestDto ReservationRequestDto) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReservationResponseDto> findAll() {
        log.info("Finding all reservations");
        final List<ReservationEntity> all = reservationRepository.findAll();
        return all.stream().map(reservationMapper::toResponse).collect(Collectors.toList());
    }

    @Override
    public ReservationResponseDto findById(Long id) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public ReservationResponseDto generateReservationTicket(Long reservationId) {
        log.info("Generating reservation ticket | Reservation id - {}", reservationId);
        final ReservationEntity reservation = reservationRepository.findById(reservationId)
                .orElseThrow(() -> new EntityNotFoundException("Reservation doesnt exist"));
        final ReservationResponseDto reservationResponseDto = reservationMapper.toResponse(reservation);
        log.info("Generating reservation ticket | Returning dto {}", reservationResponseDto);
        return reservationResponseDto;
    }
}
