package cz.fel.cvut.flightreservation.service.impl;

import cz.fel.cvut.flightreservation.domain.AircraftEntity;
import cz.fel.cvut.flightreservation.mapper.AircraftMapper;
import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.model.response.AircraftResponseDto;
import cz.fel.cvut.flightreservation.repository.AircraftRepository;
import cz.fel.cvut.flightreservation.service.AircraftService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AircraftServiceImpl implements AircraftService {

    private final AircraftMapper aircraftMapper;
    private final AircraftRepository aircraftRepository;

    @Override
    @Transactional
    public void createAircraft(AircraftRequestDto aircraftRequestDto) {
        log.info("Creating new aircraft | Request dto {}", aircraftRequestDto);
        final AircraftEntity aircraftEntity = aircraftMapper.toEntity(aircraftRequestDto);
        aircraftRepository.save(aircraftEntity);
        log.info("Creating new aircaft | New aircraft model {} with {} seats successfully created", aircraftRequestDto.getAircraftModel(),
                aircraftRequestDto.getSeatsCount());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AircraftResponseDto> findAll() {
        log.info("Returning all aircrafts from database");
        final List<AircraftEntity> all = aircraftRepository.findAll();
        return all.stream()
                .map(aircraftMapper::toResponse)
                .collect(Collectors.toList());
    }
}
