package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.model.response.AircraftResponseDto;

import java.util.List;

/**
 * Crud service layer for aircrafts
 * Aircrafts consists model of aircraft, models are different from number of seats
 */
public interface AircraftService {

    /**
     * Creates aircraft and sets seats count according to aircraft model
     * @param aircraftRequestDto
     */
    void createAircraft(AircraftRequestDto aircraftRequestDto);

    List<AircraftResponseDto> findAll();
}
