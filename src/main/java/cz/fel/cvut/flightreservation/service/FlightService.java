package cz.fel.cvut.flightreservation.service;

import cz.fel.cvut.flightreservation.criteriaAPI.SearchCriteria;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;

import java.util.List;

/**
 * Flight service
 */
public interface FlightService {

    /**
     * Method creates new flight
     * Needs to know airport ids, aircraft id.
     * @param flightRequestDto
     */
    void addFlight(FlightRequestDto flightRequestDto);

    void updateFlight(Long flightId, FlightRequestDto flightRequestDto);

    void deleteFlight(Long flightId);

    FlightResponseDto findFlight(Long flightId);

    List<FlightResponseDto> findAll();

    List<FlightResponseDto> filterFlights(List<SearchCriteria> params);
}
