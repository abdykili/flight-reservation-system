package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.ReservationEntity;
import cz.fel.cvut.flightreservation.model.request.ReservationRequestDto;
import cz.fel.cvut.flightreservation.model.response.ReservationResponseDto;
import org.mapstruct.Mapper;

@Mapper(uses = {PassengerMapper.class, FlightMapper.class, CustomerMapper.class,})
public interface ReservationMapper {

    ReservationResponseDto toResponse(ReservationEntity reservation);

    ReservationEntity toEntity(ReservationRequestDto reservationRequestDto);

}
