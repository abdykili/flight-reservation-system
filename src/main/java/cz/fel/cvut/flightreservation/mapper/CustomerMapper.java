package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.CustomerUser;
import cz.fel.cvut.flightreservation.domain.UserEntity;
import cz.fel.cvut.flightreservation.model.request.CustomerRequestDto;
import cz.fel.cvut.flightreservation.model.response.CustomerResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

    UserEntity toEntity(CustomerRequestDto customerRequestDto);

    CustomerResponseDto toResponse(UserEntity customerUser);
}
