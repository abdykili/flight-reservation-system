package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.FlightEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepository extends JpaRepository<FlightEntity, Long> {
}
