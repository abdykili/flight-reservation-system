package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.FlightEntity;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;
import org.mapstruct.Mapper;

@Mapper(uses = {AirportMapper.class, AircraftMapper.class, SeatMapper.class})
public interface FlightMapper {

    FlightEntity toEntity(FlightRequestDto flightRequestDto);

    FlightResponseDto toResponse(FlightEntity flight);


}
