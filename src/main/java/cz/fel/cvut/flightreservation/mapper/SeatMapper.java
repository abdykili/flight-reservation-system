package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.SeatEntity;
import cz.fel.cvut.flightreservation.model.response.SeatResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface SeatMapper {
    SeatResponseDto toResponse(SeatEntity seatEntity);
}
