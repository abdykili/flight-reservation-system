package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.AirportEntity;
import cz.fel.cvut.flightreservation.model.request.AirportRequestDto;
import cz.fel.cvut.flightreservation.model.response.AirportResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface AirportMapper {

    AirportEntity toEntity(AirportRequestDto airportRequestDto);

    AirportResponseDto toResponse(AirportEntity airportEntity);

}
