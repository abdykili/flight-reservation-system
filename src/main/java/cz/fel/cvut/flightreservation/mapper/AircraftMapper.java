package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.AircraftEntity;
import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.model.response.AircraftResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface AircraftMapper {

    AircraftResponseDto toResponse(AircraftEntity aircraft);

    AircraftEntity toEntity(AircraftRequestDto aircraftRequestDto);

}
