package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.CustomerUser;
import cz.fel.cvut.flightreservation.domain.UserEntity;
import cz.fel.cvut.flightreservation.model.request.CustomerRequestDto;
import cz.fel.cvut.flightreservation.model.request.ManagerRequestDto;
import cz.fel.cvut.flightreservation.model.response.CustomerResponseDto;
import cz.fel.cvut.flightreservation.model.response.ManagerResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface ManagerMapper {
    UserEntity toEntity(ManagerRequestDto managerRequestDto);

    ManagerResponseDto toResponse(UserEntity managerEntity);
}
