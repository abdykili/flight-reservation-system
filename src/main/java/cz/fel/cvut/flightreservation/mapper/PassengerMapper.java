package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.PassengerEntity;
import cz.fel.cvut.flightreservation.model.request.PassengerRequestDto;
import org.mapstruct.Mapper;

@Mapper(uses = SeatMapper.class)
public interface PassengerMapper {
    PassengerEntity toEntity(PassengerRequestDto passengerRequestDto);
}
