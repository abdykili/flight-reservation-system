package cz.fel.cvut.flightreservation.mapper;

import cz.fel.cvut.flightreservation.domain.UserEntity;
import cz.fel.cvut.flightreservation.model.request.AdminRequestDto;
import cz.fel.cvut.flightreservation.model.response.AdminResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface AdminMapper {
    UserEntity toEntity(AdminRequestDto adminRequestDto);

    AdminResponseDto toResponse(UserEntity userEntity);
}
