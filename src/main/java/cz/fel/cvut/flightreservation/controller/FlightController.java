package cz.fel.cvut.flightreservation.controller;

import cz.fel.cvut.flightreservation.criteriaAPI.SearchCriteria;
import cz.fel.cvut.flightreservation.mapper.FlightMapper;
import cz.fel.cvut.flightreservation.model.request.FlightRequestDto;
import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;
import cz.fel.cvut.flightreservation.security.TokenProvider;
import cz.fel.cvut.flightreservation.service.FlightService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/v1/flights/")
public class FlightController {
    private final String HEADER_STRING = "Authorization";

    private final String TOKEN_PREFIX = "Bearer";

    private AuthenticationManager authenticationManager;
    private TokenProvider tokenProvider;
    private final FlightService flightService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createFlight(@RequestBody FlightRequestDto flightRequestDto){
        flightService.addFlight(flightRequestDto);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @GetMapping
    public List<FlightResponseDto> findAll(){
        return flightService.findAll();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/filter")
    @ResponseStatus(HttpStatus.OK)
    public List<FlightResponseDto> filterFlights(@RequestParam(value = "search", required = false) String search){
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return flightService.filterFlights(params);
    }
}
