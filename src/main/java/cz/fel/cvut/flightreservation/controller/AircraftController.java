package cz.fel.cvut.flightreservation.controller;

import cz.fel.cvut.flightreservation.model.request.AircraftRequestDto;
import cz.fel.cvut.flightreservation.model.response.AircraftResponseDto;
import cz.fel.cvut.flightreservation.service.AircraftService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/v1/aircrafts/")
public class AircraftController {

    private final AircraftService aircraftService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void addAircraft(@RequestBody AircraftRequestDto aircraftRequestDto){
        aircraftService.createAircraft(aircraftRequestDto);
    }

    @GetMapping
    public List<AircraftResponseDto> findAll(){
        return aircraftService.findAll();
    }

}
