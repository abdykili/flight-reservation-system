package cz.fel.cvut.flightreservation.model.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class AirportResponseDto {
    String city;
    String name;
    String country;
}
