package cz.fel.cvut.flightreservation.model.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AircraftResponseDto {
    String aircraftModel;
    int seatsCount;
}
