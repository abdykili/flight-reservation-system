package cz.fel.cvut.flightreservation.model.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PassengerRequestDto {

    String nationalId;
    String name;
    String surname;
    String pass;
    String citizenship;
    LocalDate birthDate;
    Long seatId;

}
