package cz.fel.cvut.flightreservation.model.request;

import java.io.Serializable;

public enum PassengerType implements Serializable {
    ADULT,CHILD,SENIOR;

    public String getStatus() {
        return this.name();
    }
}
