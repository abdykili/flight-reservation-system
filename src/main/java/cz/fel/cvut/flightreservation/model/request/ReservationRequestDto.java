package cz.fel.cvut.flightreservation.model.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReservationRequestDto {
    String flightClass;
    Long flightId;
    List<PassengerRequestDto> passengers;
}
