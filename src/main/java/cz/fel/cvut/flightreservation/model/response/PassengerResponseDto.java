package cz.fel.cvut.flightreservation.model.response;

import cz.fel.cvut.flightreservation.domain.SeatEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PassengerResponseDto {
    String name;
    String surname;
    String pass;
    String citizenship;
    LocalDate birthDate;
    SeatResponseDto seatEntity;
    FlightResponseDto lastFlight;
    int flightsNumber;
}
