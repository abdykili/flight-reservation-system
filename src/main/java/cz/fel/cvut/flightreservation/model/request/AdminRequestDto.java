package cz.fel.cvut.flightreservation.model.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminRequestDto {
    String login;
    String password;
}


