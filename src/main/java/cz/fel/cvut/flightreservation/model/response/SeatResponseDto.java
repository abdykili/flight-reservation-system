package cz.fel.cvut.flightreservation.model.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SeatResponseDto {
    String seatName;
    String seatState;
}
