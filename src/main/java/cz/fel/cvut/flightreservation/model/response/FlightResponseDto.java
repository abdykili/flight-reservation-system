package cz.fel.cvut.flightreservation.model.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FlightResponseDto {
    LocalDateTime arrivalTime;
    LocalDateTime departureTime;
    String flightCode;
    AirportResponseDto airportDeparture;
    AirportResponseDto airportArrival;
    AircraftResponseDto aircraft;
    List<SeatResponseDto> seats;
}
