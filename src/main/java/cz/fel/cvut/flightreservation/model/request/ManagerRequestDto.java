package cz.fel.cvut.flightreservation.model.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ManagerRequestDto {

    String name;
    String surname;
    String login;
    String password;
    String phone;
    String email;
}
