package cz.fel.cvut.flightreservation.model.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReservationResponseDto {

    String flightClass;
    String reservationState;
    CustomerResponseDto customerUser;
    FlightResponseDto flight;
    Set<PassengerResponseDto> passengers;

}
