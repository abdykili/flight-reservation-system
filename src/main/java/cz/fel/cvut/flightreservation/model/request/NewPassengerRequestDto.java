package cz.fel.cvut.flightreservation.model.request;

import cz.fel.cvut.flightreservation.domain.FlightEntity;
import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class NewPassengerRequestDto {

    String nationalId;
    String name;
    String surname;
    String pass;
    String citizenship;
    LocalDate birthDate;
    FlightResponseDto lastFlight;
    int flightsNumber;
}
