package cz.fel.cvut.flightreservation.criteriaAPI;

import cz.fel.cvut.flightreservation.domain.FlightEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class FlightDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public List<FlightEntity> searchFlights(List<SearchCriteria> params) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FlightEntity> query = builder.createQuery(FlightEntity.class);
        Root r = query.from(FlightEntity.class);

        Predicate predicate = builder.conjunction();

        FlightSearchQueryCriteriaConsumer searchConsumer =
                new FlightSearchQueryCriteriaConsumer(predicate, builder, r);
        params.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        List<FlightEntity> result = entityManager.createQuery(query).getResultList();
        return result;
    }
}
