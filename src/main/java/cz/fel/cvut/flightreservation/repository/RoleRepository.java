package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(String admin);

}
