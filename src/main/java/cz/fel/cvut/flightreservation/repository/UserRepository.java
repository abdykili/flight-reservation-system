package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByLogin(String username);

    boolean existsByLogin(String login);
}
