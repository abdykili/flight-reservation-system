package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.PassengerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<PassengerEntity, Long> {
}
