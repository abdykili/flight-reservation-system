package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.SeatEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeatRepository extends JpaRepository<SeatEntity, Long> {
}
