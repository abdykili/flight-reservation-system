package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.AircraftEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AircraftRepository extends JpaRepository<AircraftEntity, Long> {
}
