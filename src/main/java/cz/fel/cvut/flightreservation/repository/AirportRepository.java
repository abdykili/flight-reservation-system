package cz.fel.cvut.flightreservation.repository;

import cz.fel.cvut.flightreservation.domain.AirportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirportRepository extends JpaRepository<AirportEntity, Long> {
}
