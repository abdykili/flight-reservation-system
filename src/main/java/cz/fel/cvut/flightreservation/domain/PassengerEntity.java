package cz.fel.cvut.flightreservation.domain;

import cz.fel.cvut.flightreservation.model.response.FlightResponseDto;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PassengerEntity extends AbstractEntity{

    String name;
    String nationalId;
    String surname;
    String pass;
    String citizenship;
    LocalDate birthDate;

    @OneToOne
    FlightEntity lastFlight;
    int flightsNumber;

    @OneToOne
    @JoinColumn(name = "fk_seat")
    SeatEntity seatEntity;

    @ManyToOne
    @JoinColumn(name="reservation_id")
    ReservationEntity reservation;

}
