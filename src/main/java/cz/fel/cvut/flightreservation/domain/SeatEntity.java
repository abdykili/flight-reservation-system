package cz.fel.cvut.flightreservation.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SeatEntity extends AbstractEntity{
    String seatName;
    @Enumerated(EnumType.STRING)
    SeatState seatState;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_flight")
    FlightEntity flight;

    @OneToOne
    @JoinColumn(name = "fk_passenger")
    PassengerEntity passengerEntity;

}
