package cz.fel.cvut.flightreservation.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FlightEntity extends AbstractEntity{

    String fromCountry;
    String toCountry;
    LocalDate arrivalDate;
    LocalDate departureDate;
    LocalDateTime arrivalTime;
    LocalDateTime departureTime;
    BigDecimal flightCost;
    String flightCode;

    @ManyToOne
    @JoinColumn(name="airport_departure_id", nullable=false)
    AirportEntity airportDeparture;

    @ManyToOne
    @JoinColumn(name="airport_arrival_id", nullable=false)
    AirportEntity airportArrival;

    @OneToMany(mappedBy = "flight",cascade = CascadeType.ALL)
    List<ReservationEntity> reservations = new ArrayList<ReservationEntity>();

    @ManyToOne
    @JoinColumn(name = "aircraft_id", referencedColumnName = "id")
    AircraftEntity aircraft;

    @OneToMany(mappedBy="flight")
    List<SeatEntity> seats = new ArrayList<SeatEntity>();
}
